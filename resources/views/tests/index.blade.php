@extends('layouts.layout')

@section('content')
  <h3 class="cyan-text"> 테스트 페이지 입니다. </h3>
  <div id="output">df</div>
@endsection


@section('script')
<script type="text/babel">
    console.log('test page', $(document));
    // Your custom script here
    const getMessage = () => "Hello World";
    document.getElementById('output').innerHTML = getMessage();
</script>
@endsection
